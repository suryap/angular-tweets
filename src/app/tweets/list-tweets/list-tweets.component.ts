import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import * as TweetSelectors from '../store/tweet.selectors';
import * as TweetActions from '../store/tweet.actions';
import { fromEvent, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { TweetModel } from '../models/tweet.model';

@Component({
  selector: 'app-list-tweets',
  templateUrl: './list-tweets.component.html',
  styleUrls: ['./list-tweets.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListTweetsComponent implements OnInit, AfterViewInit, OnDestroy {
  destroy$ = new Subject();
  @ViewChild('filter') filter: ElementRef | undefined;
  filteredHashtag = '';

  filteredTweets$ = this.store.select(TweetSelectors.selectFilteredTweets);
  hashtags$ = this.store.select(TweetSelectors.selectHashTags);
  loading$ = this.store.select(TweetSelectors.isTweetsLoading);
  tweetsCount$ = this.store.select(TweetSelectors.selectTweetsCount);

  constructor(private readonly store: Store<any>) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.store.dispatch(TweetActions.loadTweets());
    }, 1000);
  }

  ngAfterViewInit(): void {
    fromEvent(this.filter?.nativeElement, 'keyup')
      .pipe(debounceTime(150), distinctUntilChanged(), takeUntil(this.destroy$))
      .subscribe((keyupEvent: any) => {
        this.filterTweets((keyupEvent.target as HTMLInputElement).value.trim());
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterTweets(filterValue: string): void {
    this.store.dispatch(
      TweetActions.filterTweets({
        payload: { byHashTag: filterValue },
      })
    );
  }

  getHastTags(tweet: TweetModel) {
    return tweet.entities?.hashtags?.map((tag) => `#${tag.text}`);
  }

  clear() {
    this.store.dispatch(
      TweetActions.filterTweets({
        payload: { byHashTag: '' },
      })
    );
  }
}
