import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTweetsComponent } from './list-tweets/list-tweets.component';
import { StoreModule } from '@ngrx/store';
import * as fromTweetState from './store/tweet.reducer';
import { PubNubAngular } from 'pubnub-angular2';
import { EffectsModule } from '@ngrx/effects';
import { TweetEffects } from './store/tweet.effects';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [ListTweetsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    StoreModule.forFeature(
      fromTweetState.tweetStateFeatureKey,
      fromTweetState.reducer,
      { metaReducers: fromTweetState.metaReducers }
    ),
    EffectsModule.forFeature([TweetEffects]),
  ],
  exports: [ListTweetsComponent],
  providers: [PubNubAngular],
})
export class TweetsModule {}
