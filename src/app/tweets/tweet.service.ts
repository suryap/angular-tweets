import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { orderBy, take } from 'lodash-es';
import { PubNubAngular } from 'pubnub-angular2';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { TweetModel } from './models/tweet.model';

@Injectable({
  providedIn: 'root',
})
export class TweetService implements OnDestroy {
  private readonly tweets: TweetModel[] = [];
  private readonly tweets$ = new Subject<TweetModel[]>();
  constructor(private readonly pubnub: PubNubAngular) {
    this.initialize();
  }

  ngOnDestroy(): void {
    this.pubnub.unsubscribeAll();
    this.tweets$.complete();
  }

  initialize() {
    this.pubnub.init({
      publishKey: 'pub-c-fea741ae-7b8d-4660-9d2b-40a07a40fe38',
      subscribeKey: 'sub-c-78806dd4-42a6-11e4-aed8-02ee2ddab7fe',
    });

    this.pubnub.addListener({
      message: (response: any) => {
        this.tweets.push(response.message);
      },
    });

    this.pubnub.subscribe({
      channels: ['pubnub-twitter'],
    });

    setTimeout(() => {
      this.tweets$.next(
        orderBy(this.tweets, (tweet) => tweet.timestamp_ms, 'desc')
      );

      this.pubnub.unsubscribeAll();
    }, 5000);
  }

  getTweets = (): Observable<TweetModel[]> => {
    return this.tweets$;
  };
}
