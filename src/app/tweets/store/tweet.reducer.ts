import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createReducer,
  createSelector,
  MetaReducer,
  on,
} from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { TweetModel } from '../models/tweet.model';
import * as TweetsActions from './tweet.actions';

export const tweetStateFeatureKey = 'tweetState';

export interface State {
  hashtagFilter: string;
  tweets: TweetModel[];
  loading: boolean;
  errorLoadingTweets: boolean;
}

const initialState: State = {
  tweets: [],
  loading: false,
  errorLoadingTweets: false,
  hashtagFilter: '',
};

export const reducer: ActionReducer<State> = createReducer(
  initialState,
  on(TweetsActions.loadTweets, (state, _) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(TweetsActions.loadTweetsSuccess, (state, action) => {
    return {
      ...state,
      tweets: [...state.tweets, ...action.payload.tweets],
      loading: false,
    };
  }),
  on(TweetsActions.loadTweetsError, (state, _) => {
    return {
      ...state,
      loading: false,
      errorLoadingTweets: true,
    };
  }),
  on(TweetsActions.filterTweets, (state, action) => {
    return {
      ...state,
      hashtagFilter: action.payload?.byHashTag,
    };
  })
);

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
