import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, mergeMap, tap } from 'rxjs/operators';
import * as TweetActions from './tweet.actions';
import { of } from 'rxjs';
import { TweetService } from '../tweet.service';

@Injectable()
export class TweetEffects {
  loadTweets$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TweetActions.loadTweets),
      exhaustMap((_) =>
        this.tweetService.getTweets().pipe(
          map((response) => {
            return TweetActions.loadTweetsSuccess({
              payload: { tweets: response },
            });
          }),
          catchError((_) => of(TweetActions.loadTweetsError()))
        )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly tweetService: TweetService
  ) {}
}
