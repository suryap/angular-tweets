import { flatten } from '@angular/compiler';
import { createSelector, State } from '@ngrx/store';
import * as fromTweetReducer from './tweet.reducer';

const getTweetState = (state: any) => state.tweetState;

export const isTweetsLoading = createSelector(
  getTweetState,
  (tweetState: fromTweetReducer.State) => tweetState.loading
);

export const selectTweetsCount = createSelector(
  getTweetState,
  (tweetState: fromTweetReducer.State) => tweetState.tweets.length
);

export const selectTweets = createSelector(
  getTweetState,
  (tweetState: fromTweetReducer.State) => {
    return tweetState.tweets;
  }
);

export const selectFilteredTweets = createSelector(
  getTweetState,
  (tweetState: fromTweetReducer.State) => {
    if (tweetState.hashtagFilter) {
      return tweetState.tweets.filter((tweet) =>
        tweet.entities?.hashtags
          ?.map((hashtag) => hashtag.text)
          .includes(tweetState.hashtagFilter)
      );
    }

    return tweetState.tweets;
  }
);
export const selectHashTags = createSelector(
  getTweetState,
  (tweetState: fromTweetReducer.State) => {
    return flatten(
      tweetState.tweets
        .filter((tweet) => tweet.entities?.hashtags?.length > 0)
        .map((tweet) => tweet.entities?.hashtags.map((hashtag) => hashtag.text))
    );
  }
);
