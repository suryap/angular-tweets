import { createAction, props } from '@ngrx/store';
import { TweetModel } from '../models/tweet.model';

export const loadTweets = createAction('[List Tweets] Load Tweets');
export const loadTweetsSuccess = createAction(
  '[Tweets Effect] Loading Tweets Success',
  props<{ payload: { tweets: TweetModel[] } }>()
);
export const loadTweetsError = createAction(
  '[Tweets Effect] Loading Tweets Failed'
);
export const filterTweets = createAction(
  '[Filter Tweets] Load Tweets',
  props<{ payload: { byHashTag: string } }>()
);
